<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //

/* LOCALHOST */

 define('DB_NAME', 'approvedms');

 define('DB_USER', 'root');

 define('DB_PASSWORD', '');

/* PRODUCTION */

//define('DB_NAME', 'pixarw5_approvedms');
//
//define('DB_USER', 'pixarw5_approved');
//
//define('DB_PASSWORD', '!0L%b_cz3bv6');

define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xkprrvaqcwiogp83j5ax2ws3k229rygxksuh4zjjkntuqcwxpi8jehu0gtfiy07h');
define('SECURE_AUTH_KEY',  'ftih1opcwlecpqujodc0ati0fo1noiixietp1da7etmkh8zgk4qtozfx1fed2tff');
define('LOGGED_IN_KEY',    'vvgd39rzjh2hjrcksvlazuho2smgs7hwnni9bqkbtkep4wc2qsejhnbfm1ii3qqk');
define('NONCE_KEY',        'quuv4enfajm4wl3vjlmoybgfdn7p2zbtof6wxxu8kysqrpbffkrr5hizayoqvghl');
define('AUTH_SALT',        'm7p9znjy0q9hjkyewtoeegqycl1cqhpptoeahlcfe10oxf18eddwiv3dmrs9hf8m');
define('SECURE_AUTH_SALT', '5mrb8y8jinttsnobt2zz333wvtmmdrncqkzqrv8wzxtb5kgmmqdmqoaydxesumvz');
define('LOGGED_IN_SALT',   'lxluyygouecf2iqjdtlklp9guw0fjuqrkqokr92uusbxoynlf6brsms8wpbclp7l');
define('NONCE_SALT',       'lyla4uwvkg8bcx5qs4syycauwzkvcysssbz4vjivhybya6rcptmpwkpi8fhjgtso');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

define('FS_METHOD', 'direct');

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
