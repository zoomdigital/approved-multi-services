<?php
if($post->post_name!="contact"){

?>

<section id="parallax3" style="background-image: url(&quot;<?php echo of_get_option('testimonial_bg');?>&quot;); background-size: 110%; background-position: 45.986% 22.993%;" class="parallax">
  <div class="section-shade sep-top-2x sep-bottom-1x">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
          <div class="section-title">
            <h3 class="light small-space">What Our Clients Say</h3>
           </div>

        </div>

        <?php
        $mypost = array( 'post_type' => 'testimonial', );
        $loop = new WP_Query( $mypost );
        ?>
        <?php while ( $loop->have_posts() ) : $loop->the_post();?>


          <div class="col-md-4 testi">
              <?php echo the_content();?>
              <figure>
<?php echo the_post_thumbnail();?>
                <h5>
<?php echo get_the_title();?>
                 </h5>  </figure>

        </div>

<?php endwhile;?>

      </div>
      <?php
        // Reset query to prevent conflicts
        wp_reset_query();
        ?>
    </div>
  </div>
</section>

<?php } ?>




<section id="contacts" class="sep-top-2x sep-bottom-1x">
  <div class="container">

    <div class="row">
      <div class="col-md-6 sep-top-lg">
          &nbsp;
      </div>
        <div class="col-md-6 sep-top-lg">
            <h4> Get a quote</h4>
        </div>
      <div class="col-md-6 sep-top-lg">
        <!--Start icon box-->
        <div class="icon-box icon-xs sep-bottom-xs icon-gradient">

          <div class="icon-box-content">
            <h6 class="upper info-title">Address</h6>
            <p><?php echo of_get_option('address');?><br/>
</p>
          </div>
        </div>

        <div class="icon-box icon-xs sep-bottom-xs icon-gradient">
          <div class="icon-box-content">
            <h6 class="upper info-title">Email</h6>
            <p><a href="mailto:hello@approvedmultiservices.com">hello@approvedmultiservices.com<a/></p>
          </div>
        </div>
        <div class="icon-box icon-xs sep-bottom-xs icon-gradient">
          <div class="icon-box-content">
            <h6 class="upper info-title">Phone </h6>
            <p><a href="tel:<?php echo of_get_option('phone_number');?>"><?php echo of_get_option('phone_number');?></a></p>
          </div>
        </div>

        <!--End icon box-->
      </div>
      <div class="col-md-6 sep-top-lg">
        <div class="contact-form">
          <div id="successMessage" style="display:none" class="alert alert-success text-center">
            <p><i class="fa fa-check-circle fa-2x"></i></p>
            <p>Thanks for sending your message! We'll get back to you shortly.</p>
          </div>
          <div id="failureMessage" style="display:none" class="alert alert-danger text-center">
            <p><i class="fa fa-times-circle fa-2x"></i></p>
            <p>There was a problem sending your message. Please, try again.</p>
          </div>
          <div id="incompleteMessage" style="display:none" class="alert alert-warning text-center">
            <p><i class="fa fa-exclamation-triangle fa-2x"></i></p>
            <p>Please complete all the fields in the form before sending.</p>
          </div>
<?php echo do_shortcode('[contact-form-7 id="505" title="Contact form 1"]'); ?>
         </div>
      </div>
    </div>
  </div>
</section>


   <section>
  <div id="map-canvas" style="height:500px">
    <script>
      var
        lat = 51.359343,
        lon = -0.140725,
        infoText = [
          '<div style="white-space:nowrap">',
            '<h5>Approved Multi Services</h5>',
            'Stafford Studios, 129A Stafford Rd<br>',
            'Wellington, SM6',
          '</div>'
        ],
        mapOptions = {
          scrollwheel: false,
          markers: [
            { latitude: lat, longitude: lon, html: infoText.join('') }
          ],
          icon: {
            image: '<?php echo of_get_option('map_pin');?>',
            iconsize: [295,210],
            iconanchor: [12, 65],
          },
          latitude: lat,
          longitude: lon,
          zoom: 16
        };
    </script>
  </div>
</section>

<div id="back_to_top"><a href="#" class="fa fa-arrow-up fa-lg"></a></div>

</div>

    <script  src="<?php echo get_template_directory_uri();?>/js/vendor.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap-extend.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/wmljs.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/jquery.infinitescroll.min.js"></script>
    <script>
    $('.bxslider').bxSlider({
        mode: 'fade',
        auto: true
    });
    </script>

<?php get_footer();?>

</body>
</html>
