<?php get_header(); ?>

<section class="demo-2">
    <div class="banner" style="background-image: url(<?php echo home_url(); ?>/wp-content/uploads/2016/05/services.jpg);">
        <div class="col-md-1">&nbsp;</div>
        <div class="banner-content text-center">
            <div class="col-md-12 no_padding">
                <h1 class="banner-title">Our Blog</h1>
            </div>
        </div>
    </div>
</section>

<style>
.wmle_container .wmle_item .wpme_image img {
    border-radius: 0px !important;
    box-shadow: none !important;
    width: 100%;
}
.wmle_post_meta{
    display: none;
}
.wmle_post_title a{
    color:#006CB7 !important;
    font-size: 1.3em;
}
.wmle_post_excerpt p{
    padding: 2%;
    font-size: 15px;
    min-height: 138px;
}
.wmle_container .wmle_item .wmle_post_excerpt {
    padding-top: 0px;
}
</style>

<section class="sep-top-1x sep-bottom-1x">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" style="padding: 4%;">
                <div data-wow-delay="0.5s" class="text-center wow bounceInLeft animated" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInLeft;">

                    <style>
                    .wmle_container .wmle_item{border:1px solid #e5e5e5; margin:5px; padding:5px;-webkit-box-shadow: 0px 0px 3px -1px #959595;box-shadow: 0px 0px 3px -1px #959595;}
                    .wmle_container .wmle_item .wpme_image{ text-align:center;}
                    .wmle_container .wmle_item .wpme_image img{border-radius:0px !important; box-shadow:none !important;}
                    .wmle_container .wmle_item .wmle_post_meta{color:#a5a4a2; font-size:11px;line-height:1.5; padding-bottom:6px;}
                    .wmle_container .wmle_item .wmle_post_meta a{ color:inherit; text-decoration:none;}
                    .wmle_container .wmle_item .wmle_post_meta a:hover{ text-decoration:underline;}
                    .wmle_container .wmle_item .wmle_post_title{ font-size:12px; color:#a5a4a2; line-height:1.5; padding-bottom:6px;border-bottom:1px solid #f1f1f1; border-top:1px solid #f1f1f1; padding-top:5px; padding-bottom:5px; font-weight:bold;}
                    .wmle_container .wmle_item .wmle_post_title a{ color:inherit; text-decoration:none;}
                    .wmle_container .wmle_item .wmle_post_excerpt{font-size:12px; color:#a5a4a2; padding-top:10px; padding-bottom:10px;}
                    .wmle_container .wmle_item .wmle_post_excerpt p{ line-height:1.5;}
                    .wmle_container .wmle_item .wmle_post_excerpt p:last-child{ padding-bottom:0px; margin-bottom:0px;}
                    .wmle_loadmore .wmle_loadmore_btn{ display:inline-block; padding:7px 30px;border:2px solid #454545; margin:5px;color:#454545; text-decoration:none;text-transform:uppercase;}
                    </style>

                    <div class="wmle_container responsive" id="wmle_container" data-load-status="ready" data-seed="616238841" style="position: relative; height: 949.468px;" data-page="2">
                        <div class="wmle_item_holder col3" style="position: absolute; left: 0px; top: 0px;"></div>

                        <?php if (have_posts()) : while (have_posts()) : the_post();?>
                            <div class="wmle_item_holder col3" style="position: absolute; left: 0px; top: 0px;">
                                <div class="wmle_item">
                                    <div class="wpme_image">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail( 'medium' ); ?>
                                        </a>
                                    </div>
                                    <div class="wmle_post_meta">
                                        <a href="<?php the_permalink(); ?>/#respond">0 Response</a>
                                    </div>
                                    <div class="wmle_post_title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </div>
                                    <div class="wmle_post_excerpt">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; endif; ?>

                    </div>

                    <script>
                    jQuery(document).ready( function() {
                    	jQuery('.wmle_loadmore_btn').trigger('click');
                    });
                    </script>

                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
