<?php
/**
 * Template Name: Services
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */



 get_header();

?>







<?php if (have_posts()) : while (have_posts()) : the_post();?>

<section class="demo-2">

  <div class="banner" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));?>);">
    <div class="col-md-1">
            &nbsp;
                </div>
      <div class="banner-content text-center">


        <div class="col-md-12 no_padding">
                <h1 class="banner-title"><?php echo get_the_title();?></h1>

                </div>


            </div>




  </div>

</section>

<section class="sep-top-1x sep-bottom-1x">
  <div class="container">
    <div class="row">

<div class="col-md-12 text-center" style="padding: 4%;">
              <div data-wow-delay="0.5s" class="text-center wow bounceInLeft animated" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceInLeft;">

                <p class="lead" style="font-size: 25px;">  <?php echo the_content();?></p>
              </div>
            </div>





      <div class="col-md-12" style="padding-top: 12px;">
<?php
$mypost = array( 'post_type' => 'servicess', );
$loop = new WP_Query( $mypost );
?>
<?php while ( $loop->have_posts() ) : $loop->the_post();?>


          <div class="col-md-4 services_item">
 <figure>
 <?php
 if (class_exists('MultiPostThumbnails')) :
     MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image');
endif;
?>
                  </figure>              <h2><?php echo get_the_title();?></h2>
              <div class="services_content">
<?php echo the_excerpt();?>
                  <a href="<?=get_permalink($post->ID)?>">Learn More</a>
              </div>
          </div>

<?php endwhile;?>


    </div>
    <?php
    	// Reset query to prevent conflicts
    	wp_reset_query();
    	?>

  </div>
</section>


<?php endwhile; endif; ?>


<?php


 get_footer();
?>
