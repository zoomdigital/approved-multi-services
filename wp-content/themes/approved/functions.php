<?php
//include("options-framework/options.php");
/**
 * Approved Services functions and definitions
 *
 * @package approvedservies
 * @since approvedservies 1.0
 */

wp_enqueue_script('jquery');


function sendMessage(){


$name = $_POST['senderName'];
$phone = $_POST['senderPhone'];
$email = $_POST['senderEmail'];
  $website = $_POST['senderWebsite'];
$comments = $_POST['comment'];


   $emailTo = of_get_option('email');
 $subject = 'Contact Form Submission from '.$name;
 $body = "Name: $name nnEmail: $email nnPhone: $phone nnWebsite: $website nnComments: $comments";
 $headers = 'From: My Site <'.$emailTo.'>' . "rn" . 'Reply-To: ' . $email;

 mail($emailTo, $subject, $body, $headers);




echo "success";
die();
}

add_action('wp_ajax_sendMessage', 'sendMessage');
add_action('wp_ajax_nopriv_sendMessage', 'sendMessage'); // not really needed


include_once 'inc/theme_functions.php';


 function approved_styles() {
 	// Add custom fonts, used in the main stylesheet.
 	wp_enqueue_style( 'style-css', get_template_directory_uri().'/style.css', array(), null );

 	// Add Genericons, used in the main stylesheet.
 }
 add_action( 'wp_enqueue_scripts', 'approved_styles' );


/**
 * First, let's set the maximum content width based on the theme's design and stylesheet.
 * This will limit the width of all uploaded images and embeds.
 */
 add_theme_support( 'title-tag' );

 /*
  * Loads the Options Panel
  *
  * If you're loading from a child theme use stylesheet_directory
  * instead of template_directory
  */

 define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/options-framework/inc/' );
 require_once dirname( __FILE__ ) . '/options-framework/inc/options-framework.php';
 // Loads options.php from child or parent theme
 $optionsfile = locate_template( 'inc/options.php' );
 load_template( $optionsfile );

 /*
  * This is an example of how to add custom scripts to the options panel.
  * This one shows/hides the an option when a checkbox is clicked.
  *
  * You can delete it if you not using that option
  */
 add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

 function optionsframework_custom_scripts() { ?>

 <script type="text/javascript">
 jQuery(document).ready(function() {

 	jQuery('#example_showhidden').click(function() {
   		jQuery('#section-example_text_hidden').fadeToggle(400);
 	});

 	if (jQuery('#example_showhidden:checked').val() !== undefined) {
 		jQuery('#section-example_text_hidden').show();
 	}

 });
 </script>

 <?php
 }

 /*
  * This is an example of filtering menu parameters
  */

 /*
 function prefix_options_menu_filter( $menu ) {
 	$menu['mode'] = 'menu';
 	$menu['page_title'] = __( 'Hello Options', 'textdomain');
 	$menu['menu_title'] = __( 'Hello Options', 'textdomain');
 	$menu['menu_slug'] = 'hello-options';
 	return $menu;
 }

 add_filter( 'optionsframework_menu', 'prefix_options_menu_filter' );
 */






if ( ! function_exists( 'approvedservies_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function approvedservies_setup() {

    /**
     * Make theme available for translation.
     * Translations can be placed in the /languages/ directory.
     */

    /**
     * Add default posts and comments RSS feed links to <head>.
     */
    add_theme_support( 'automatic-feed-links' );

    /**
     * Enable support for post thumbnails and featured images.
     */
    add_theme_support( 'post-thumbnails' );

    /**
     * Add support for two custom navigation menus.
     */
    register_nav_menus( array(
        'primary-menu'   => __( 'Primary Menu', 'approvedservies' )

            ) );


            // Add ID and CLASS attributes to the first <ul> occurence in wp_page_menu
    /**
     * Enable support for the following post formats:
     * aside, gallery, quote, image, and video
     */
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
}
endif; // approvedservies_setup
add_action( 'after_setup_theme', 'approvedservies_setup' );


//testimonial
function approved_testimonial_init() {
    $args = array(
      'label' => 'Testimonial',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'rewrite' => array('slug' => 'testimonial'),
        'query_var' => true,
        'menu_icon' => 'dashicons-video-alt',
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail','custom-fields','editor')
        );
    register_post_type( 'testimonial', $args );
}
add_action( 'init', 'approved_testimonial_init' );


//Services
function approved_services_init() {
    $args = array(
      'label' => 'Services',
      'name'=> 'services',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'rewrite' => array('slug' => 'services'),
        'query_var' => true,
        'menu_icon' => 'dashicons-video-alt',
        'supports' => array(
            'title',
            'excerpt',
            'thumbnail','custom-fields','editor')
        );
    register_post_type( 'servicess', $args );
}
add_action( 'init', 'approved_services_init' );


class CSS_Menu_Maker_Walker extends Walker {

  var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

  function start_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "\n$indent<ul aria-labelledby='menu_item_Portfolio' class='dropdown-menu'>\n";
  }

  function end_lvl( &$output, $depth = 0, $args = array() ) {
    $indent = str_repeat("\t", $depth);
    $output .= "$indent</ul>\n";
  }

  function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
    $class_names = $value = '';
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    /* Add active class */
    if(in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
      unset($classes['current-menu-item']);
    }
    $ddown = " ";
$attributes = " ";
    /* Check for children */
    $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
    if (!empty($children)) {
      $classes[] = 'dropdown';
      $attributes .=  ' data-hover="dropdown"';
      $attributes .= ! empty( $item->url )        ? ' data-ref="'   . esc_attr( $item->url        ) .'"' : '';
      $attributes .=  ' class="dropdown-toggle"';
$ddown= '<span class="caret"></span>';
    }

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  .= ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';



    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= $ddown;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }

  function end_el( &$output, $item, $depth = 0, $args = array() ) {
    $output .= "</li>\n";
  }
}
