<?php

/* Template Name: Home */

get_header();

?>

<section class="demo-2">

  <?php echo do_shortcode("[sp_responsiveslider]");?>

</section>

<section class="sep-top-1x sep-bottom-1x">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div data-wow-delay="0.5s" class="text-center wow bounceInLeft">
          <?php echo the_content();?>
        </div>
      </div>



      <div class="col-md-8 col-md-offset-2">
     <div data-wow-delay="0.5s" class="text-center wow bounceInLeft">
       <h3 class="upper_t">Our Services</h3>
       </div>
   </div>


      <div class="col-md-12" style="padding-top: 12px;">
<?php
$mypost = array( 'post_type' => 'servicess', );
$loop = new WP_Query( $mypost );
?>
<?php while ( $loop->have_posts() ) : $loop->the_post();?>


          <div class="col-md-4 services_item">
              <figure><?php if (class_exists('MultiPostThumbnails')) :

MultiPostThumbnails::the_post_thumbnail(get_post_type(), 'secondary-image');

endif;
?>
                  </figure>
              <h2><?php echo get_the_title();?></h2>
              <div class="services_content">
<?php echo the_excerpt();?>
                  <a href="<?=get_permalink($post->ID)?>">Learn More</a>
              </div>
          </div>

<?php endwhile;?>


    </div>
    <?php
    	// Reset query to prevent conflicts
    	wp_reset_query();
    	?>

  </div>
</section>





<section id="about" class="sep-top-2x sep-bottom-2x">
  <div class="container">
    <div class="row">
      <div class="col-md-6 about_content ">

<?php echo of_get_option('about_section');?>
      </div>
        <div class="col-md-6" style="    padding: 0px;">
        <div class="screen" ><img src="<?php echo of_get_option('about_section_img');?>" alt="" width="577" height="433" class="img-responsive"></div>
      </div>
    </div>

  </div>
</section>



<?php
get_footer();

?>
