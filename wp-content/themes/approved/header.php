<html class="no-js">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link href='https://fonts.googleapis.com/css?family=Montserrat:400' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/vendor.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/jquery.bxslider.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">

    <script async src="<?php echo get_template_directory_uri();?>/js/modernizr.js"></script>

    <script type="text/javascript">
    jQuery('document').ready(function(){
        jQuery('#ssendBtn').click(function(){
            var newSendMessage = jQuery(this).serialize();
            jQuery.ajax({
                type:"POST",
                url: "<?php echo home_url(); ?>/wp-admin/admin-ajax.php",
                data: newSendMessage,
                success:function(data){
                    alert(data);
                }
            });
        });
    });
    </script>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-78476171-1', 'auto');
    ga('send', 'pageview');
    </script>

    <?php wp_head();?>

</head>
<body>
    <div id="load"></div>
    <div class="page">

      <nav>

          <div class="top-header">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                  <ul class="top">
                      <li><img src="<?php echo get_template_directory_uri();?>/img/email.png"/> <a href="mailto:hello@approvedmultiservices.com">hello@approvedmultiservices.com</a></li>
                      <li><img src="<?php echo get_template_directory_uri();?>/img/call.png"/> <a href="tel:<?php echo of_get_option('phone_number');?>"><?php echo of_get_option('phone_number');?></a></li>
                      <li><img src="<?php echo get_template_directory_uri();?>/img/time.png"/> <?php echo of_get_option('opening_hours');?></li>
                  </ul>
              </div>
            </div>
          </div>
        </div>
      </nav>


      <nav id="main-navigation" role="navigation" class="navbar navbar-fixed-top navbar-standard">



        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle"><i class="fa fa-align-justify fa-lg"></i></button>
            <a href="<?php echo site_url(); ?>" class="navbar-brand"><img src="<?php echo of_get_option('logo_url');?>" alt="" class="logo-white"><img src="<?php echo of_get_option('logo_url');?>" alt="" class="logo-dark"></a>
          </div>

            <div class="navbar-collapse collapse" style="background: white; border-bottom: 5px solid #d3d3d3;">

            <button type="button" class="navbar-toggle"><i class="fa fa-close fa-lg"></i></button>
                    <!-- istanzio un id univoco-->

        <?php    wp_nav_menu(array(
              'menu' => 'Primary Menu',
              'container_id' => 'mm',
              'items_wrap' => '<ul class="nav yamm navbar-nav navbar-left main-nav">%3$s</ul>' ,
              'walker' => new CSS_Menu_Maker_Walker()
            ));
            ?>
          </div>
        </div>
      </nav>
