<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'theme-textdomain'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __( 'One', 'theme-textdomain' ),
		'two' => __( 'Two', 'theme-textdomain' ),
		'three' => __( 'Three', 'theme-textdomain' ),
		'four' => __( 'Four', 'theme-textdomain' ),
		'five' => __( 'Five', 'theme-textdomain' )
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __( 'French Toast', 'theme-textdomain' ),
		'two' => __( 'Pancake', 'theme-textdomain' ),
		'three' => __( 'Omelette', 'theme-textdomain' ),
		'four' => __( 'Crepe', 'theme-textdomain' ),
		'five' => __( 'Waffle', 'theme-textdomain' )
	);

	
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

	$options[] = array(
		'name' => __( 'Theme Settings', 'theme-textdomain' ),
		'type' => 'heading'
	);




	$options[] = array(
		'name' => __( 'Logo URL', 'theme-textdomain' ),
		'desc' => __( 'Please enter logo url.', 'theme-textdomain' ),
		'id' => 'logo_url',
		'std' => '',
		'type' => 'upload'
	);

  $options[] = array(
		'name' => __( 'Email', 'theme-textdomain' ),
		'desc' => __( 'Please enter Contact Emal.', 'theme-textdomain' ),
		'id' => 'email',
		'std' => '',
		'type' => 'text'
	);

  $options[] = array(
		'name' => __( 'Phone Number', 'theme-textdomain' ),
		'desc' => __( 'Please enter Phone Number.', 'theme-textdomain' ),
		'id' => 'phone_number',
		'std' => '',
		'type' => 'text'
	);

  $options[] = array(
		'name' => __( 'Opening Hours', 'theme-textdomain' ),
		'desc' => __( 'Please enter Opening Hours.', 'theme-textdomain' ),
		'id' => 'opening_hours',
		'std' => '',
		'type' => 'text'
	);
  $options[] = array(
		'name' => __( 'Address', 'theme-textdomain' ),
		'desc' => __( 'Please enter address.', 'theme-textdomain' ),
		'id' => 'address',
		'std' => '',
		'type' => 'textarea'
	);
  $options[] = array(
		'name' => __( 'HomePage About Section', 'theme-textdomain' ),
		'desc' => __( 'Please enter Content for Homepage About section.', 'theme-textdomain' ),
		'id' => 'about_section',
		'std' => '',
		'type' => 'textarea'
	);

  $options[] = array(
		'name' => __( 'HomePage About Section Image', 'theme-textdomain' ),
		'desc' => __( 'Please  select image for Homepage About section.', 'theme-textdomain' ),
		'id' => 'about_section_img',
		'std' => '',
		'type' => 'upload'
	);

  $options[] = array(
		'name' => __( 'HomePage - Testimonial Background Image', 'theme-textdomain' ),
		'desc' => __( 'Please select image for testimonial background.', 'theme-textdomain' ),
		'id' => 'testimonial_bg',
		'std' => '',
		'type' => 'upload'
	);

  $options[] = array(
    'name' => __( 'Footer Map Pin Image', 'theme-textdomain' ),
    'desc' => __( 'Please select image for Map Pin.', 'theme-textdomain' ),
    'id' => 'map_pin',
    'std' => '',
    'type' => 'upload'
  );
	/**
	 * For $settings options see:
	 * http://codex.wordpress.org/Function_Reference/wp_editor
	 *
	 * 'media_buttons' are not supported as there is no post to attach items to
	 * 'textarea_name' is set by the 'id' you choose
	 */
	return $options;
}
