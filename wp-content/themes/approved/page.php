<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

<section class="demo-2">

  <div class="banner" style="background-image: url(<?php echo wp_get_attachment_url(get_post_thumbnail_id($post->ID));?>);">
    <div class="col-md-1">
            &nbsp;
                </div>
      <div class="banner-content text-center">


        <div class="col-md-12 no_padding">
                <h1 class="banner-title"><?php echo get_the_title();?></h1>

                </div>


            </div>




  </div>

</section>


<section class="">
  <div class="container">
    <div class="row">


     <div class="col-md-8 content" style="padding: 4%;">

<?php echo the_content();?>


      </div>

<div class="col-md-4 getquote" >
           <div class="sign-up-sec">
               <div id="successMessage" style="display:none" class="alert alert-success text-center">
            <p><i class="fa fa-check-circle fa-2x"></i></p>
            <p>Thanks for sending your message! We'll get back to you shortly.</p>
          </div>
          <div id="failureMessage" style="display:none" class="alert alert-danger text-center">
            <p><i class="fa fa-times-circle fa-2x"></i></p>
            <p>There was a problem sending your message. Please, try again.</p>
          </div>
          <div id="incompleteMessage" style="display:none" class="alert alert-warning text-center">
            <p><i class="fa fa-exclamation-triangle fa-2x"></i></p>
            <p>Please complete all the fields in the form before sending.</p>
          </div>
               <form id="contactForm" action="http://www.approvedmultiservices.com/wp-admin/admin-ajax.php" method="post" class="validate" novalidate="novalidate" style="display: block;">
                              <h2>Get a Free Quote</h2>
                              <input placeholder="Your Name" required="" name="senderName" class="text-field">
                              <input placeholder="Your Email" required="" name="senderEmail" class="text-field">
                              <input placeholder="Your Phone" name="phone" class="text-field">
                              <textarea placeholder="Message" name="comment" rows="5" class="text-field"></textarea>
                            <input type="hidden" name="action" value="sendMessage"/>
                              <!-- Start: Common Button -->
                              <div class="common-buttons">
                                  <button id="submit" type="submit" class="view-more">GET IN TOUCH</button>
                              </div>
                              <!-- End: Common Button -->
</form>

                          </div>



      </div>





    </div>

  </div>
</section>

<?php endwhile; endif; ?>


<?php


 get_footer();
?>
