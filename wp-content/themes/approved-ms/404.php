<?php
/*
*   Template Name: About Us
*/

get_header();
?>

    <div class="inner-banner">
        <div class="frame">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1>404! Not Found</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-contents about-page">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h4>Sorry the page you are looking for is not here.</h4>

                </div>
            </div>
        </div>
    </div>

<?php get_footer(); ?>