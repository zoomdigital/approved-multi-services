<?php global $amsOptions; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-106180962-1', 'auto');
        ga('send', 'pageview');
    </script>

    <?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo"><?php the_custom_logo(); ?></div>
                </div>
                <div class="col-md-8">
                    <div class="top-bar">
                        <a href="mailto:<?php echo esc_attr( $amsOptions['email_address'] ); ?>"><i class="fa fa-envelope"></i> <?php echo esc_html( $amsOptions['email_address'] ); ?></a>
                        <a href="tel:<?php echo esc_attr( $amsOptions['phone_no'] ); ?>"><i class="fa fa-phone"></i> <?php echo esc_html( $amsOptions['phone_no'] ); ?></a>
                    </div>
                    <nav class="navbar">
                        <button class="navbar-toggler hidden-lg-up navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="fa fa-list"></span>
                        </button>
                        <?php
                        wp_nav_menu( array(
                            'theme_location' => 'main',
                            'menu_class'     => 'nav collapse navbar-collapse navbar-toggleable-md',
                            'menu_id'        => 'navbarNav',
                            'container'      => ''
                        ) );
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>
