<?php global $amsOptions; ?>

<?php
if( !is_page_template( 'tmp-contact.php' ) ) {
    ?>
    <div class="testimonial-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h3>What Our Clients Says</h3>
                    <div class="testimonials-carousel">

                        <?php
                        $testimonial_args = array(
                            'post_type' => array( 'testimonial' ),
                            'posts_per_page' => -1
                        );
                        $testimonials = new WP_Query( $testimonial_args );
                        if ( $testimonials->have_posts() ) {
                            while ($testimonials->have_posts()) {
                                $testimonials->the_post();
                                ?>
                                <div class="quote">
                                    <blockquote>
                                        <?php the_content(); ?>
                                    </blockquote>
                                    <div class="meta">
                                        <?php the_post_thumbnail( 'full', ['class' => 'avatar'] ); ?>
                                        <strong><?php the_title(); ?></strong>
                                        <span><?php echo rwmb_meta( 'rw_testimonial_designation' ); ?></span>
                                    </div>
                                </div>
                                <?php
                            }
                            wp_reset_postdata();
                        }
                        ?>
                    </div>
                    <div class="testimonial-arrows"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>

    <footer>
        <div class="container">
            <div class="row">

                <?php
                if( !is_page_template( 'tmp-contact.php' ) ) {
                    ?>
                    <div class="col-md-6">
                        <div class="widget-left">
                            <h3 class="widget-title"><?php echo esc_html( $amsOptions['form_title'] ); ?></h3>
                            <form action="<?php echo esc_url( get_template_directory_uri() ); ?>/mail-handler.php" method="post" id="footer_form">
                                <fieldset>
                                    <input type="text" name="name" placeholder="Your Name" required>
                                    <input type="email" name="email" placeholder="Your Email" required>
                                    <input type="tel" name="phone" placeholder="Your Phone" required>
                                </fieldset>
                                <textarea name="message" id="message" cols="30" rows="10">Message</textarea>
                                <input type="submit" class="btn" value="Submit">
                                <div id="output"></div>
                            </form>
                            <div id="output"></div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="<?php echo is_page_template( 'tmp-contact.php' ) ? 'col-md-12' : 'col-md-6'; ?>">

                    <?php if( !is_page_template( 'tmp-contact.php' ) ) { ?>
                        <div id="map_canvas"></div>
                    <?php } ?>

                    <div class="widget-right <?php echo is_page_template( 'tmp-contact.php' ) ? 'text-center' : ''; ?>">

                        <nav class="footer-nav">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'footer',
                                'menu_class'     => 'nav',
                                'container'      => '',
                                'depth'          => 1
                            ) );
                            ?>
                        </nav>
                        <p class="copyrights"><?php echo esc_html( $amsOptions['copyrights'] ); ?></p>

                    </div>

<!--                    <p class="social-nav">-->
<!--                        <a href="--><?php //echo esc_url( $amsOptions['facebook_url'] ); ?><!--"></a>-->
<!--                    </p>-->

                </div>

            </div>
        </div>
    </footer>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {

            // Footer Form
            var options = {
                target:        '#output',
                beforeSubmit:  function showRequest(formData, jqForm, options) {
                    $('#output').html( '<i class="fa fa-spinner fa-spin"></i>' );
                },
                success:       function showResponse(responseText, statusText, xhr, $form)  {}
            };
            $('#footer_form').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
            });

            $('#contact_form').submit(function() {
                $(this).ajaxSubmit(options);
                return false;
            });
            function initMap() {
                var uluru = {lat: <?php echo esc_html( $amsOptions['map_lat'] ); ?>, lng: <?php echo esc_html( $amsOptions['map_lng'] ); ?>};
                var map = new google.maps.Map(document.getElementById('map_canvas'), {
                    zoom: 17,
                    center: uluru,
                    scrollwheel: false
                });
                var marker = new google.maps.Marker({
                    position: uluru,
                    map: map,
                    icon: '<?php echo get_template_directory_uri(); ?>/assets/images/contact-map-marker.png'
                });
            }
            initMap();


            // Testimonials Carousel
            $('.testimonials-carousel').slick({
                dots: false,
                infinite: false,
                speed: 2000,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 5000,
                appendArrows: '.testimonial-arrows',
                prevArrow: '<a href="#" class="slick-prev"><i class="fa fa-angle-left"></i></a>',
                nextArrow: '<a href="#" class="slick-next"><i class="fa fa-angle-right"></i></a>'
            });

            <?php
            if( is_front_page() ) {
                // Caption Carousel
                ?>
                $('.caption-carousel').slick({
                    dots: false,
                    infinite: true,
                    speed: 500,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    autoplay: true,
                    autoplaySpeed: 2500,
                    fade: true,
                    cssEase: 'linear',
                    appendArrows: '.caption-arrows',
                    prevArrow: '<a href="#" class="slick-prev"><i class="fa fa-angle-left"></i></a>',
                    nextArrow: '<a href="#" class="slick-next"><i class="fa fa-angle-right"></i></a>'
                });
                <?php
            }
            ?>

            <?php
            if( is_page_template( 'tmp-employees.php' ) ) {
                // Find Employees Form
                ?>
                var options = {
                    target:        '#output',
                    beforeSubmit:  function showRequest(formData, jqForm, options) {
                        $('#output').html( '<i class="fa fa-spinner fa-spin"></i>' );
                    },
                    success:       function showResponse(responseText, statusText, xhr, $form)  {}
                };
                $('#git_form').submit(function() {
                    $(this).ajaxSubmit(options);
                    return false;
                });
                $('.boxes a').on( 'click', function(e){
                    e.preventDefault();
                } );
                <?php
            }
            ?>

            <?php
            if( is_page_template( 'tmp-jobs.php' ) || is_search() ) {
                // Find Jobs Form, CV Upload and PopUp
                ?>
                $('#cvForm').submit(function() {
                    $(this).ajaxSubmit({
                        target:        '#output2',
                        beforeSubmit:  function showRequest(formData, jqForm, options) {
                            $('#output2').html( '<i class="fa fa-spinner fa-spin"></i>' );
                        },
                        success:       function showResponse(responseText, statusText, xhr, $form)  {}
                    });
                    return false;
                });
                $('#uploadCV').on( 'click', function(e) {

                    e.preventDefault();
                    $('#popUp').fadeIn( 'slow' );
                    $('body').css( 'overflow', 'hidden' );
                    close_popup();

                } );
                function close_popup() {

                    $('.close-btn').on( 'click', function(e) {

                        e.preventDefault();
                        $('#popUp').fadeOut( 'fast' );
                        $('#popUpJob').fadeOut( 'fast' );
                        $('body').css( 'overflow', 'auto' );

                    } );

                }
                $('.selectpicker').selectpicker({
                    dropupAuto: false
                });
                <?php
            }
            ?>

            <?php
            if( is_singular( 'ars_jobs' ) ) {
                ?>
                $('#applyForm').submit(function() {
                    $(this).ajaxSubmit({
                        target:        '#output3',
                        beforeSubmit:  function showRequest(formData, jqForm, options) {
                            $('#output3').html( '<i class="fa fa-spinner fa-spin"></i>' );
                        },
                        success:       function showResponse(responseText, statusText, xhr, $form)  {}
                    });
                    return false;
                });
                <?php
            }
            ?>

        } );
    </script>


</body>
</html>
