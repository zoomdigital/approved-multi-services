<?php

function ars_setup() {

    load_theme_textdomain( 'ars' );

    add_theme_support( 'automatic-feed-links' );

    add_theme_support( 'title-tag' );

    add_theme_support( 'post-thumbnails' );

    register_nav_menus( array(
        'main'    => __( 'Main Menu', 'ars' ),
        'footer' => __( 'Footer Menu', 'ars' ),
    ) );

    add_theme_support( 'html5', array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

    add_theme_support( 'post-formats', array(
        'aside',
        'image',
        'video',
        'quote',
        'link',
        'gallery',
        'audio',
    ) );

    add_theme_support( 'custom-logo', array(
        'width'       => 328,
        'height'      => 67,
        'flex-width'  => true,
    ) );

    add_image_size( 'job-image', 100, 120, true );

    add_theme_support( 'customize-selective-refresh-widgets' );

}
add_action( 'after_setup_theme', 'ars_setup' );

function ars_scripts() {

    wp_enqueue_style( 'ars-style', get_stylesheet_uri() );
    wp_enqueue_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css' );
    wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ) );
    wp_enqueue_style( 'ars-global', get_theme_file_uri( '/assets/css/global.css' ) );

    if( is_front_page() ) {
        wp_enqueue_style( 'ars-home', get_theme_file_uri( '/assets/css/home.css' ) );
    }

    if( is_page_template( 'tmp-about.php' ) || is_404() || is_singular( 'servicess' ) ) {
        wp_enqueue_style( 'ars-inner', get_theme_file_uri( '/assets/css/inner.css' ) );
    }

    wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
    wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

    wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js', array( 'jquery' ), '1.0', true );
    wp_enqueue_script( 'jquery-form', get_theme_file_uri( '/assets/js/jquery.form.min.js' ), array( 'jquery' ), '1.0', true );

    if( is_page_template( 'tmp-contact.php' ) ) {
        wp_enqueue_style( 'ars-contact', get_theme_file_uri( '/assets/css/contact.css' ) );
    }

    wp_enqueue_script( 'gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA0TSGvFiOdPnG3IPUWEWvu6o2wJmo7As4', array(), '', true );

    wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css' );
    wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js', array( 'jquery' ), '', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }

}
add_action( 'wp_enqueue_scripts', 'ars_scripts' );

require get_parent_theme_file_path( '/inc/post-types.php' );

require get_parent_theme_file_path( '/inc/post-taxonomies.php' );

require get_parent_theme_file_path( '/inc/plugins.php' );

require get_parent_theme_file_path( '/inc/theme-options.php' );

require get_parent_theme_file_path( '/inc/theme-meta-boxes.php' );