<?php
/*
*   Template Name: About Us
*/

global $amsOptions;

get_header();
?>

<?php if( have_posts() ) : while( have_posts() ) the_post(); ?>

    <?php
    if( has_post_thumbnail() ) {
        $bg = 'style="background:url('.get_the_post_thumbnail_url().') no-repeat;background-size:cover;"';
    } else {
        $bg = 'style="background: url('.get_template_directory_uri().'/assets/images/inner-banner.jpg) no-repeat;"';
    }
    ?>

    <div class="inner-banner">
        <div class="frame" <?php echo $bg; ?>>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-contents about-page">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <?php the_content(); ?>
                </div>
                <div class="col-md-5">
                    <form action="<?php echo get_template_directory_uri(); ?>/mail-handler.php" method="post">
                        <h3>Get a Free Quote</h3>
                        <input type="text" name="name" placeholder="Your name" class="half" required>
                        <input type="email" name="email" placeholder="Your email" class="half last" required>
                        <input type="tel" name="phone" placeholder="Your Phone" class="full" required>
                        <textarea name="message" id="" cols="30" rows="10" placeholder="Your message" required></textarea>
                        <input type="submit" name="submit" class="btn" value="Submit">
                        <div id="output"></div>
                    </form>
<!--                    <a href="--><?php //echo esc_url( $amsOptions['facebook_url'] ); ?><!--" class="social_btn"></a>-->
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php get_footer(); ?>