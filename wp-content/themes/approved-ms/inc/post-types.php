<?php

function ams_testimonials_post_type() {

    $labels = array(
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'ams' ),
        'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'ams' ),
        'menu_name'             => __( 'Testimonials', 'ams' )
    );
    $rewrite = array(
        'slug'                  => 'testimonial',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __( 'Testimonial', 'ams' ),
        'description'           => '',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail' ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type( 'testimonial', $args );

}
add_action( 'init', 'ams_testimonials_post_type', 0 );

function ams_services_post_type() {

    $labels = array(
        'name'                  => _x( 'Services', 'Post Type General Name', 'ams' ),
        'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'ams' ),
        'menu_name'             => __( 'Services', 'ams' )
    );
    $rewrite = array(
        'slug'                  => 'services',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $args = array(
        'label'                 => __( 'Service', 'ams' ),
        'description'           => '',
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' ),
        'taxonomies'            => array(),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 20,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'               => $rewrite,
        'capability_type'       => 'post',
    );
    register_post_type( 'servicess', $args );

}
add_action( 'init', 'ams_services_post_type', 0 );