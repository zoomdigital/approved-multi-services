<?php
/**
 * ReduxFramework Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 */

if ( ! class_exists( 'Redux' ) ) {
    return;
}

$opt_name = "amsOptions";

$theme = wp_get_theme();

$args = array(
    'opt_name'             => $opt_name,
    'display_name'         => $theme->get( 'Name' ),
    'display_version'      => $theme->get( 'Version' ),
    'menu_type'            => 'menu',
    'allow_sub_menu'       => true,
    'menu_title'           => __( 'Approved MS Options', 'ars' ),
    'page_title'           => __( 'Approved MS Options', 'ars' ),
    'google_api_key'       => '',
    'google_update_weekly' => false,
    'async_typography'     => true,
    'admin_bar'            => true,
    'admin_bar_icon'       => 'dashicons-portfolio',
    'admin_bar_priority'   => 50,
    'global_variable'      => '',
    'dev_mode'             => false,
    'update_notice'        => true,
    'customizer'           => true
);

$args['share_icons'][] = array(
    'url'   => 'http://www.umairishtiaq.com',
    'title' => 'Get in Touch',
    'icon'  => 'el el-envelope'
);

Redux::setArgs( $opt_name, $args );

Redux::setSection( $opt_name, array(
    'title'            => 'Header',
    'id'               => 'header',
    'customizer_width' => '500px',
    'desc'             => '',
    'fields'           => array(
        array(
            'id'       => 'email_address',
            'type'     => 'text',
            'title'    => 'Top Bar Email',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'hello@approvedmultiservices.com'
        ),
        array(
            'id'       => 'phone_no',
            'type'     => 'text',
            'title'    => 'Top Bar Phone',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '+44(0)20 8935 5628'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => 'Home',
    'id'               => 'home',
    'customizer_width' => '500px'
) );

//Redux::setSection( $opt_name, array(
//    'title'            => 'Main Banner',
//    'id'               => 'home_banner',
//    'customizer_width' => '500px',
//    'desc'             => '',
//    'subsection'       => true,
//    'fields'           => array(
//        array(
//            'id'       => 'banner',
//            'type'     => 'background',
//            'title'    => 'Banner Image',
//            'subtitle' => '',
//            'desc'     => '',
//            'background-color' => false,
//            'background-attachment' => false,
//            'output'   => '.main-banner',
//            'preview'  => true,
//            'default'  => array(
//                'background-image' => get_template_directory_uri() . '/assets/images/main-banner.jpg',
//                'background-repeat' => 'no-repeat',
//                'background-size' => 'cover',
//                'background-position' => 'center top',
//            )
//        )
//    )
//) );

Redux::setSection( $opt_name, array(
    'title'            => 'Why Us',
    'id'               => 'whyus',
    'customizer_width' => '500px',
    'desc'             => '',
    'subsection'       => true,
    'fields'           => array(
        array(
            'id'       => 'whyus_title',
            'type'     => 'text',
            'title'    => 'Title',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<strong>Why Us?</strong>'
        ),
        array(
            'id'       => 'whyus_content',
            'type'     => 'editor',
            'title'    => 'Content',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<h6>We take away the hassle of dealing with multiple suppliers and can simplify your projects without any additional cost.</h6><p>You’ll work with a dedicated project manager who will be your point of contact from start to finish and will be fully accountable for delivering on time and on budget.</p>'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => 'Services',
    'id'               => 'services',
    'customizer_width' => '500px',
    'desc'             => '',
    'subsection'       => true,
    'fields'           => array(
        array(
            'id'       => 'fservices_title',
            'type'     => 'text',
            'title'    => 'Title',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'Our Services'
        ),
        array(
            'id'       => 'fservices_count',
            'type'     => 'spinner',
            'title'    => 'Services Count',
            'subtitle' => '',
            'desc'     => 'No of services boxes to show in home page section.',
            'default'  => '8',
            'min'      => '2',
            'step'     => '3',
            'max'      => '12',
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => 'About',
    'id'               => 'about',
    'customizer_width' => '500px',
    'desc'             => '',
    'subsection'       => true,
    'fields'           => array(
        array(
            'id'       => 'about_title',
            'type'     => 'text',
            'title'    => 'Title',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'About Approved Multi Services'
        ),
        array(
            'id'       => 'about_content',
            'type'     => 'editor',
            'title'    => 'Content',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<ul>
                    <li>Over 10 years experience working with schools, local authorities and businesses in London and the South East</li>
                    <li> we provide no obligation surveys and quotes and are competitive on price with no hidden costs</li>
                    <li>our services are tailored to meet your exacting needs and we take immense pride in the projects we manage</li>
                </ul>'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => 'Contact',
    'id'               => 'contact',
    'customizer_width' => '500px',
    'desc'             => '',
    'fields'           => array(
        array(
            'id'       => 'contact_title',
            'type'     => 'text',
            'title'    => 'Form Title',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'Get a Quote'
        ),
        array(
            'id'       => 'address',
            'type'     => 'textarea',
            'title'    => 'Address',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<h6>Address</h6><p>129 Stafford Rd, Wallington,<br> SM6 9BN</p>'
        ),
        array(
            'id'       => 'address_mail',
            'type'     => 'textarea',
            'title'    => 'Emails',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<p><i class="fa fa-envelope"></i> <a href="mailto:contact@approvedmultiservices.com">hello@approvedmultiservices.com</a></p>'
        ),
        array(
            'id'       => 'address_phone',
            'type'     => 'textarea',
            'title'    => 'Phones',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '<p><i class="fa fa-phone"></i> <a href="tel:+442086691110">+44(0) 20 8669 1110</a></p>'
        ),
        array(
            'id'       => 'map_lat',
            'type'     => 'text',
            'title'    => 'Map Latitude',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '51.359393'
        ),
        array(
            'id'       => 'map_lng',
            'type'     => 'text',
            'title'    => 'Map Longitude',
            'subtitle' => '',
            'desc'     => '',
            'default'  => '-0.140783'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'            => 'Footer',
    'id'               => 'footer',
    'customizer_width' => '500px',
    'desc'             => '',
    'fields'           => array(
        array(
            'id'       => 'form_title',
            'type'     => 'text',
            'title'    => 'Contact Form Title',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'Send us a message'
        ),
        array(
            'id'       => 'copyrights',
            'type'     => 'editor',
            'title'    => 'Copyrights',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'Copyright &copy; ' . date( 'Y' ) . 'Approved Multi Services. All rights reserved.'
        ),
        array(
            'id'       => 'facebook_url',
            'type'     => 'text',
            'title'    => 'Facebook URL',
            'subtitle' => '',
            'desc'     => '',
            'default'  => 'http://www.facebook.com'
        ),
    )
) );