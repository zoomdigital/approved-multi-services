<?php

require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'ars_register_required_plugins' );

function ars_register_required_plugins() {

	$plugins = array(
		array(
			'name'      => 'Redux Options',
			'slug'      => 'redux-framework',
			'required'  => true,
            'force_activate' => true,
            'force_deactivation' => true
		),
        array(
            'name'      => 'Meta Box',
            'slug'      => 'meta-box',
            'required'  => true,
            'force_activate' => true,
            'force_deactivation' => true
        ),
	);

	$config = array(
		'id'           => 'ars',
		'default_path' => '',
		'menu'         => 'ars-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => true,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
	);

	tgmpa( $plugins, $config );
}
