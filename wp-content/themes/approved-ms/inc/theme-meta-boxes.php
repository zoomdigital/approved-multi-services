<?php
add_filter( 'rwmb_meta_boxes', 'ams_register_meta_boxes' );

function ams_register_meta_boxes( $meta_boxes ) {

    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'services',
        'title'      => 'Service Settings',
        'post_types' => array( 'servicess' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => 'Upload Icon',
                'desc'  => '',
                'id'    => $prefix . 'service_icon',
                'type'  => 'image_advanced',
                'std'   => '',
                'clone' => false,
                'max_file_uploads' => '1'
            ),
            array(
                'name'  => 'Upload Slider Image',
                'desc'  => '',
                'id'    => $prefix . 'service_slide',
                'type'  => 'image_advanced',
                'std'   => '',
                'clone' => false,
                'max_file_uploads' => '1'
            ),
        )
    );

    $meta_boxes[] = array(
        'id'         => 'testimonial',
        'title'      => 'Testimonial Settings',
        'post_types' => array( 'testimonial' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'fields' => array(
            array(
                'name'  => 'Designation',
                'desc'  => '',
                'id'    => $prefix . 'testimonial_designation',
                'type'  => 'text',
                'std'   => '',
                'clone' => false
            ),
        )
    );

    return $meta_boxes;

}