<?php

function ars_jobs_taxonomies() {

    // Locations

    $labels = array(
        'name'                       => _x( 'Locations', 'Taxonomy General Name', 'ars' ),
        'singular_name'              => _x( 'Location', 'Taxonomy Singular Name', 'ars' ),
        'menu_name'                  => __( 'Locations', 'text_domain' )
    );
    $rewrite = array(
        'slug'                       => 'jobs-location',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'ars_job_locations', array( 'ars_jobs' ), $args );

    // Categories

    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'ars' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'ars' ),
        'menu_name'                  => __( 'Categories', 'text_domain' )
    );
    $rewrite = array(
        'slug'                       => 'jobs-category',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => false,
        'rewrite'                    => $rewrite,
    );
    register_taxonomy( 'ars_job_categories', array( 'ars_jobs' ), $args );

}
add_action( 'init', 'ars_jobs_taxonomies', 0 );