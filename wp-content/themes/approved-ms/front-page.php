<?php

global $amsOptions;

get_header();

?>

    <div class="main-banner">
        <div class="caption-carousel">
            <?php
            $services_args = array(
                'post_type' => array( 'servicess' ),
                'posts_per_page' => -1
            );
            $services = new WP_Query( $services_args );
            if ( $services->have_posts() ) {
                while ( $services->have_posts() ) {
                    $services->the_post();
                    $image = rwmb_meta( 'rw_service_slide', 'size=full&limit=1' );
                    ?>
                    <div class="caption" style="background-image: url(<?php echo $image[0]['url']; ?>);">
                        <h1><?php the_title(); ?></h1>
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="btn">Read More</a>
                    </div>
                    <?php
                }
                wp_reset_postdata();
            }
            ?>
        </div>
        <div class="caption-arrows"></div>
        <span class="arrow"></span>
    </div>

<div class="whyus-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <figure><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/about.jpg" alt="" class="img-fluid"></figure>
            </div>
            <div class="col-md-7">
                <article>
                    <h2><?php echo wp_kses_post( $amsOptions['whyus_title'] ); ?></h2>
                    <?php echo wp_kses_post( $amsOptions['whyus_content'] ); ?>
                    <a href="<?php home_url( '/' ) ?>contact" class="btn">Get a free quote</a>
                </article>
            </div>
        </div>
    </div>
</div>

<div class="featured-services">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><?php echo wp_kses_post( $amsOptions['fservices_title'] ); ?></h3>
            </div>

            <div class="col-md-8">
            <?php

            if( !empty( $amsOptions['fservices_count'] ) ) {
                $pp = $amsOptions['fservices_count'];
            } else {
                $pp = 8;
            }

            $args = array(
                'post_type' => array( 'servicess' ),
                'posts_per_page' => $pp,
                'orderby' => 'menu_order',
                'order' => 'ASC'
            );

            $featured_jobs = new WP_Query( $args );

            if ( $featured_jobs->have_posts() ) {
                echo '<div class="service-row">';
                $count = 1;
                while ( $featured_jobs->have_posts() ) {
                    $featured_jobs->the_post();
                    $icon = rwmb_meta( 'rw_service_icon', 'type=image&limit=1', get_the_ID() );
                    ?>
                    <div class="service-box">
                        <?php echo '<img src="' . esc_url( $icon[0]['full_url'] ) . '"  alt="' . esc_attr( $icon[0]['alt'] ) . '" class="icon">'; ?>
                        <h5><?php the_title(); ?></h5>
                        <a href="<?php the_permalink(); ?>" class="link"></a>
                    </div>
                    <?php
                    if( $count == 2 ) echo '</div><div class="service-row">';
                    $count++;
                }
                echo '</div>';
            } else {
                ?>
                <h6>Sorry! No Job Available.</h6>
                <?php
            }

            wp_reset_postdata();

            ?>
            </div>
        </div>
    </div>
</div>

<div class="about-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <article>
                    <h3><?php echo esc_html( $amsOptions['about_title'] ); ?></h3>
                    <?php echo wp_kses_post( $amsOptions['about_content'] ); ?>
                </article>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>