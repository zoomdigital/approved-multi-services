<?php
/*
*   Template Name: Contact Us
*/

global $amsOptions;

get_header();

?>

<?php if( have_posts() ) : while( have_posts() ) the_post(); ?>

    <?php
    if( has_post_thumbnail() ) {
        $bg = 'style="background:url('.get_the_post_thumbnail_url().') no-repeat;background-size:cover;"';
    } else {
        $bg = 'style="background: url('.get_template_directory_uri().'/assets/images/inner-banner.jpg) no-repeat;"';
    }
    ?>

    <div class="contact-banner">
        <div class="frame" <?php echo $bg; ?>>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-contents">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section class="row">

                        <div class="col-md-7">
                            <div class="contact-form">
                                <h3 class="widget-title"><?php echo wp_kses_post( $amsOptions['contact_title'] ); ?></h3>
                                <form action="<?php echo esc_url( get_template_directory_uri() ); ?>/mail-handler.php" method="post" id="contact_form">
                                    <fieldset>
                                        <input type="text" name="name" placeholder="Your Name" required class="half">
                                        <input type="email" name="email" placeholder="Your Email" required class="half last">
                                        <input type="tel" name="phone" placeholder="Your Phone" required class="full">
                                    </fieldset>
                                    <textarea name="message" id="message" cols="30" rows="10">Message</textarea>
                                    <input type="submit" class="btn" value="Submit">
                                </form>
                                <div id="output"></div>
                            </div>
                        </div>

                        <div class="col-md-5">
                            <div class="address-wrap">
                                <address class="address">
                                    <?php echo wp_kses_post( $amsOptions['address'] ); ?>
                                </address>
                                <address class="email">
                                    <?php echo wp_kses_post( $amsOptions['address_mail'] ); ?>
                                </address>
                                <address class="phone">
                                    <?php echo wp_kses_post( $amsOptions['address_phone'] ); ?>
                                </address>
                            </div>
                        </div>

                    </section>
                </div>
            </div>
        </div>
    </div>

    <div id="map_canvas"></div>

<?php endif; ?>

<?php get_footer(); ?>